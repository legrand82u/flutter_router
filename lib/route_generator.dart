import 'package:flutter/material.dart';
import './main.dart';

//On décentralise le process de routing du reste du code, pour y voir plus clair, et, si erreur de routing il y a, on sait d'où elle vient
class RouteGenerator {
  //Fonction créant les routes, le type de sortie et de l'argument sont données avec la fonction de route 'onGenerateRoute'
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => FirstPage());
      case '/secondPage':
        if (args is String) {
          return MaterialPageRoute(
            builder: (_) => SecondPage(
              data: args,
            ),
          );
        }
        //Si pas de string en argument, erreur
        return _errorRoute();
      default:
        //Si pas une route connue --> On sort de ce que l'on connait --> erreur
        return _errorRoute();
    }
  }

  //Page d'erreur, crée comme ça car tuto.
  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text(
            'Error',
          ),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
