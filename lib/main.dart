import 'package:flutter/material.dart';
import 'package:flutter_router/route_generator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Routing',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute:
          '/', // On ne passe plus par home, mais par "initialRoute" + nom de la route
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Routing App'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text(
              'First Page !',
              style: TextStyle(
                fontSize: 50,
              ),
            ),
            RaisedButton(
              onPressed: () {
                //PushNamed car on utilise des routes nommées
                Navigator.of(context).pushNamed('/secondPage',
                    arguments: 'Hello from the first page !');
                // Avec la version là, (fonction onGenerateRoute), on peut passer des arguments qui peuvent changer,
                // ce qui n'est pas le cas avec les routes définies dans "routes"
              },
              child: Text('Go to second'),
            ),
          ],
        ),
      ),
    );
  }
}

class SecondPage extends StatelessWidget {
  final String data;

  SecondPage({@required this.data});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Page'),
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          Text(
            'Second Page.',
            style: TextStyle(fontSize: 50),
          ),
          Text(
            data,
            style: TextStyle(fontSize: 20),
          ),
        ],
      )),
    );
  }
}
