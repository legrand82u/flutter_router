# Routes nommées sous flutter

Le principe, est de créer des routes nommées, au préalable, afin d'éviter, comme l'autre principe des routes, de les créer au vol. Ici, on peut soit utiliser 'route:' afin de créer les routes, cependant, on ne peut pas y mettre des arguments changeants. Pour le faire, il faut utiliser 'onGenerateRoute:' qui permet, avec une fonction anonyme, ou, comme dans ce projet, avec une classe, de générer les routes. 

Tutoriel suivi : 'https://www.youtube.com/watch?v=nyvwx7o277U'